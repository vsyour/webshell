
**一键脚本目录**
### 1. 用ZSH 构建一个使用WEB连接的终端 
[ZSH](#ZSH)

### 2. 搭建Docker环境 
[Docker](#Docker)

---
## 搭建WEB 终端 
<a id="ZSH">ZSH</a>
```bash
wget -O zsh.sh https://bit.ly/3u52FhI > /dev/null 2>&1 && chmod +x zsh.sh && ./zsh.sh

```
---
## 搭建Docker环境
<a id="Docker">Docker</a>
```bash
wget -O docker.sh https://bit.ly/installDocker > /dev/null 2>&1 && chmod +x docker.sh && ./docker.sh

```
---