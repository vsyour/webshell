rm -rf ngrok ngrok.zip ng.sh > /dev/null 2>&1
echo "======================="
echo "Download ngrok"
echo "======================="
wget -O ngrok.zip https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip > /dev/null 2>&1
unzip ngrok.zip > /dev/null 2>&1
read -p "Paste Ngrok Authtoken: " CRP
./ngrok authtoken $CRP 

# from https://bitbucket.org/hacktechlife/myvps/raw/aef8eac9133e1b6e9636421e5a7bdc037d55ea2e/GCngrok.sh
# short url: https://bit.ly/install-ngrok